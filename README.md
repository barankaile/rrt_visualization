Produces a visualization of how Rapidly-exploring random tree search works. 
Allows the user to choose a start and end point, as well as place objects that can complicate the search space.
After the goal has been reached, it will automatically alter the search parameters and use a genetic algorithm to produce a visualization of how RRT can be modified.